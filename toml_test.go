package main

import (
	"bytes"
	"testing"

	"github.com/BurntSushi/toml"
)

type TOMLNameData struct {
	Name string
}

func TestEncodeTOMLToString(t *testing.T) {
	b := &bytes.Buffer{}
	encoder := toml.NewEncoder(b)
	tomlData := TOMLNameData{Name: "my-name"}
	err := encoder.Encode(tomlData)
	if err != nil {
		t.Errorf("Encoding error %q", err)
	}
	wanted := "Name = \"my-name\"\n"
	got := b.String()
	if got != wanted {
		t.Errorf("Got %q, wanted %q", got, wanted)
	}
}

func TestDecodeStringToTOML(t *testing.T) {
	tomlString := "Name = \"my-name\"\n"
	tomlData := TOMLNameData{}
	_, err := toml.Decode(tomlString, &tomlData)
	if err != nil {
		t.Errorf("Got error: %q", err)
	}
	if tomlData.Name != "my-name" {
		t.Errorf("Got %q, wanted %q", tomlData.Name, "my-name")
	}
}

func TestDecodeStringToTOMLWithLowercaseKey(t *testing.T) {
	tomlString := "name = \"my-name\"\n"
	tomlData := TOMLNameData{}
	_, err := toml.Decode(tomlString, &tomlData)
	if err != nil {
		t.Errorf("Got error: %q", err)
	}
	if tomlData.Name != "my-name" {
		t.Errorf("Got %q, wanted %q", tomlData.Name, "my-name")
	}
}

func TestDecodeStringToTOMLWithRandomcaseKey(t *testing.T) {
	tomlString := "NaMe = \"my-name\"\n"
	tomlData := TOMLNameData{}
	_, err := toml.Decode(tomlString, &tomlData)
	if err != nil {
		t.Errorf("Got error: %q", err)
	}
	if tomlData.Name != "my-name" {
		t.Errorf("Got %q, wanted %q", tomlData.Name, "my-name")
	}
}

func TestInvalidTOMLReturnsError(t *testing.T) {
	tomlString := "Name ="
	tomlData := TOMLNameData{}
	_, err := toml.Decode(tomlString, &tomlData)
	if err == nil {
		t.Errorf("Got success, expected error")
	}
}
