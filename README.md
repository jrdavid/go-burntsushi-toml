[![pipeline status](https://gitlab.com/jrdavid/go-burntsushi-toml/badges/master/pipeline.svg)](https://gitlab.com/jrdavid/go-burntsushi-toml/-/commits/master)

# Learning Tests for TOML Library in Golang

This is a suite of learning tests for a [TOML library](https://github.com/BurntSushi/toml)
in Golang.
